alphabet = ' ابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی'


def encrypt_pc(plain_text: str, key: int):
    """Encrypts a Persian plain text to Caesar-cipher by a key."""
    cipher_text = ''
    for char in plain_text:
        opened_index = alphabet.find(char)
        closed_index = opened_index + key  # Locking the index using key
        if closed_index > len(alphabet) - 1:
            closed_index = closed_index - len(alphabet)
        cipher_text = cipher_text + alphabet[closed_index]
    return cipher_text


def decrypt_pc(text_cipher: str, key: int):
    """Decrypts a Persian cipher text to plain text by the given key. """
    text_plain = ''
    for chara in text_cipher:
        close_index = alphabet.find(chara)
        open_index = close_index - key  # Unlocking the index using key
        if open_index < 0:
            open_index = open_index + len(alphabet)
        text_plain = text_plain + alphabet[open_index]
    return text_plain
