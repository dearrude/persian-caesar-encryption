import modules

e_d = 'nonsense'  # Because it is nonsense :)
while e_d != '0':
    e_d = input("""Do you want to encrypt or decrypt? (e/d)
    Type 0 to exit the program.""")
    if e_d == 'e':
        en_plain = input('Enter you Persian plain text:')
        en_key = int(input('Now enter a key: (from 0 to 30)'))
        en_cipher = modules.encrypt_pc(en_plain, en_key)
        print('Your text has been encrypted: {}'.format(en_cipher))
    elif e_d == 'd':
        de_cipher = input('Enter your Persian cipher text:')
        de_key = int(input("Now enter your key:"))
        de_plain = modules.decrypt_pc(de_cipher, de_key)
        print('Your text has been decrypted: {}'.format(de_plain))
